package com.rutvik.tip;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;
import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private Context context;
    private Activity activity;
    private ArrayList view_id, view_amt, view_tip, view_ans;

    CustomAdapter(Activity activity, Context context, ArrayList view_id, ArrayList view_amt, ArrayList view_tip,
                  ArrayList view_ans){
        this.activity = activity;
        this.context = context;
        this.view_id = view_id;
        this.view_amt = view_amt;
        this.view_tip = view_tip;
        this.view_ans = view_ans;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.my_row, parent, false);
        return new MyViewHolder(view);
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void onBindViewHolder(@NonNull final MyViewHolder holder, final int position) {
        holder.view_id_txt.setText(String.valueOf(view_id.get(position)));
        holder.view_amt_txt.setText(String.valueOf(view_amt.get(position)));
        holder.view_tip_txt.setText(String.valueOf(view_tip.get(position)));
        holder.view_ans_txt.setText(String.valueOf(view_ans.get(position)));
        //Recyclerview onClickListener



    }

    @Override
    public int getItemCount() {
        return view_id.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder {

        TextView view_id_txt, view_amt_txt, view_tip_txt, view_ans_txt;
        LinearLayout mainLayout;

        MyViewHolder(@NonNull View itemView) {
            super(itemView);
            view_id_txt = itemView.findViewById(R.id.view_id_txt);
            view_amt_txt = itemView.findViewById(R.id.view_amt_txt);
            view_tip_txt = itemView.findViewById(R.id.view_tip_txt);
            view_ans_txt = itemView.findViewById(R.id.view_ans_txt);
            mainLayout = itemView.findViewById(R.id.mainLayout);

        }

    }

}

