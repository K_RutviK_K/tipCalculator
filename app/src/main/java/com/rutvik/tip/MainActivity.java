package com.rutvik.tip;

import androidx.appcompat.app.AppCompatActivity;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {

    EditText number1;
    EditText number2;
    Button Add_button;
    Button Reset_button;
    Button Log_button;
    TextView result;
    TextView tip;
    TextView amt;
    int ans=0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        // by ID we can use each component which id is assign in xml file
        number1=(EditText) findViewById(R.id.editText_first_no);
        number2=(EditText) findViewById(R.id.editText_second_no);
        Add_button=(Button) findViewById(R.id.add_button);
        Reset_button=(Button) findViewById(R.id.reset_button);
        Log_button=(Button) findViewById(R.id.log_button);
        result = (TextView) findViewById(R.id.textView_answer);
        tip = (TextView) findViewById(R.id.textView_tip);
        amt = (TextView) findViewById(R.id.textView_amt);


        Reset_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                number1.setText("");
                number2.setText("");
                result.setText("");
                tip.setText("");
                amt.setText("");

            }

        });






        // Add_button add clicklistener
        Add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    Double num1 = Double.parseDouble(number1.getText().toString());
                    Double num2 = Double.parseDouble(number2.getText().toString());

                    double tipValue = (num2 * num1) / 100;
                    double sum = num1 + tipValue;

                    tip.setText(Double.toString(tipValue));

                    result.setText(Double.toString(sum));

                    amt.setText(Double.toString(num1));


                MyDatabaseHelper myDB = new MyDatabaseHelper(MainActivity.this);
                myDB.addlog(number1.getText().toString().trim(),
                        number2.getText().toString().trim(),
                        result.getText().toString().trim()
                );

            }



        });


    }

    public void btnlogs(View view) {
        startActivity(new Intent(getApplicationContext(),viewActivity.class));
    }


    boolean isValidUser() {
        boolean isValid = true;
        if (TextUtils.isEmpty(number1.getText().toString())) {
            number1.setError(getString(R.string.error1));
        }

        if (TextUtils.isEmpty(number2.getText().toString())) {
            isValid = false;
            number2.setError(getString(R.string.error2));
        }


        return isValid;
    }



}
